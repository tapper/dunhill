angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $ionicModal, $timeout , $rootScope) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

})

.controller('MainCtrl', function($scope,$http,$rootScope,$timeout) 
{
	$scope.$on('$ionicView.enter', function(e) 
	{
		$timeout(function() 
		{

			$scope.setPackage = function(nm)
			{
				 $rootScope.Package = nm;
				 window.location.href="#/app/register"
			}
			
		}, 300);
	});
})



.controller('RegisterCtrl', function($scope, $stateParams,$rootScope,$timeout,$ionicLoading,$http,$ionicPopup) 
{
	

	  $scope.iframeHeight = '300px';
	
	//$scope.$on('$ionicView.enter', function(e) 
	//{
		//$timeout(function() 
		//{		
		
			if ($rootScope.Package == 0)
				$scope.pageBackground = "img/new/back2.jpg";
			
			else if ($rootScope.Package == 1)
				$scope.pageBackground = "img/new/background2.jpg";
			
			else if ($rootScope.Package == 2)
				$scope.pageBackground = "img/new/background3.jpg";
			
			$scope.fields = 
			{
				"name" : "",
				"phone" : "",
				"mail" : "",
				"address" : "",
				"sex" : "",
				"favorite" : "",
				"brand" : "",
				"birthdate" : "",
				"agree" : false
			}
			
			$scope.agreeTerms = function(type)
			{
				$scope.fields.agree = type;
			}
			
			$scope.meetsMinimumAge = function(birthDate,year,month,day, minAge) {
				var tempDate = new Date((parseInt(year) + parseInt(minAge)), month, day);
				
				return (tempDate <= new Date());
			}

			
			
			$scope.goWin = function()
			{
				$scope.year = $scope.fields.birthdate.split("-");



				if ($scope.fields.name =="")
				{
					$ionicPopup.alert({
					 title: 'יש למלא שם מלא',
					 template: '',
					 okText : 'אישור'
				   });						
				}
				else if ($scope.fields.phone =="" && $scope.fields.mail =="")
				{
					$ionicPopup.alert({
					 title: 'יש למלא דוא"ל או טלפון',
					 template: '',
					 okText : 'אישור'
				   });						
				}
				
				else if ($scope.fields.address =="")
				{
					$ionicPopup.alert({
					 title: 'יש להזין כתובת מגורים',
					 template: '',
					 okText : 'אישור'
				   });					
				}
				
				else if ($scope.fields.sex =="")
				{
					$ionicPopup.alert({
					 title: 'יש לבחור מין',
					 template: '',
					 okText : 'אישור'
				   });						
				}
				/*
				else if ($scope.fields.favorite =="")
				{
					$ionicPopup.alert({
					 title: 'יש לבחור תחביב',
					 template: '',
					 okText : 'אישור'
				   });						
				}
				*/
				else if ($scope.fields.brand =="")
				{
					$ionicPopup.alert({
					 title: 'יש לבחור מותג סיגריות',
					 template: '',
					 okText : 'אישור'
				   });						
				}
				else if ($scope.fields.birthdate =="")
				{
					$ionicPopup.alert({
					 title: 'יש למלא תאריך לידה',
					 template: '',
					 okText : 'אישור'
				   });						
				}	

				else if (!$scope.meetsMinimumAge($scope.fields.birthdate,$scope.year[0],$scope.year[1],$scope.year[2], 18)) 
				{

					$ionicPopup.alert({
					 title: 'הינך חייב להיות מעל גיל 18',
					 template: '',
					 okText : 'אישור'
				   });	
				}
				
				/*else if ($scope.fields.agree == false)
				{
					$ionicPopup.alert({
					 title: 'יש להסכים לתנאי השימוש',
					 template: '',
					 okText : 'אישור'
				   });						
				}		*/		

				
				else
				{
					$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';


			
					$ionicLoading.show({
					  template: 'טוען...<ion-spinner icon="spiral"></ion-spinner>'
					});	

					if ($scope.fields.agree == true)
						$scope.agreeField = 1;
					else
						$scope.agreeField = 0;
					
			

					send_params = 
					{
						"name" : $scope.fields.name,
						"phone" : $scope.fields.phone,
						"mail" : $scope.fields.mail,
						"address" : $scope.fields.address,
						"sex" : $scope.fields.sex,
						"favorite" : $scope.fields.favorite,
						"brand" : $scope.fields.brand,
						"birthdate" : $scope.fields.birthdate,
						"tabletid" : parseInt(tabletId),
						"package" : $rootScope.Package, // 0 = dunhill ,  1 = pallmall , 2 = golden vargina
						"agree" : String($scope.agreeField),
						"send" : 1
					}

					
					$http.post($rootScope.Host+'/register.php',send_params)
					.success(function(data, status, headers, config)
					{
						$ionicLoading.hide();
						window.location.href="#/app/main";

						
					})
					.error(function(data, status, headers, config)
					{
						$ionicLoading.hide();
						window.location.href="#/app/main";
					});		

					$scope.fields.name = '';
					$scope.fields.phone = '';
					$scope.fields.address = '';
					$scope.fields.mail = '';
					$scope.fields.birthdate = '';
				}	
			}
		//}, 300);
	//});
})



    .controller('FirstCtrl', function($scope, $stateParams,$rootScope,$timeout,$ionicLoading,$http,$ionicPopup)
    {
    	$scope.fields =
		{
			"tabletid" : parseInt(tabletId),			
			"underage" : "0",
			"parentunder" : "0",
			"nonsmoker" : "0",
			"smoker" : "0",
			"uninformed" : "0",
			"informed" : "0",
			"send" : "1"
		}

		$scope.checkSelected = function (value) 
		{
			
			switch(value) {
				case 0:
					$scope.fields.underage = "1";
					$scope.fields.parentunder = "0";
					$scope.fields.parentunder = "0";
					$scope.fields.smoker = "0";	
					$scope.fields.uninformed = "0";
					$scope.fields.informed = "0";
					
					break;
				case 1:
					$scope.fields.underage = "0";
					break;

				case 2:
					$scope.fields.parentunder = "1";
					$scope.fields.nonsmoker = "0";
					$scope.fields.underage = "0";
					$scope.fields.smoker = "0";	
					$scope.fields.uninformed = "0";
					$scope.fields.informed = "0";
					break;
					

				case 3:
					$scope.fields.parentunder = "0";
					break;

				case 4:
					$scope.fields.nonsmoker = "1";
					$scope.fields.parentunder = "0";
					$scope.fields.underage = "0";
					$scope.fields.parentunder = "0";
					$scope.fields.smoker = "0";		
					$scope.fields.uninformed = "0";
					$scope.fields.informed = "0";
					break;

				case 5:
					$scope.fields.nonsmoker = "0";
					break;

				case 6:
					$scope.fields.smoker = "1";
					$scope.fields.nonsmoker = "0";
					$scope.fields.parentunder = "0";
					$scope.fields.underage = "0";
					$scope.fields.parentunder = "0";
					$scope.fields.uninformed = "0";
					$scope.fields.informed = "0";
					break;

				case 7:
					$scope.fields.smoker = "0";
					break;					
					
					
					
				case 8:
					$scope.fields.uninformed = "1";
					$scope.fields.informed = "0";
					break;						
					
					
				case 9:
					$scope.fields.uninformed = "0";
					break;						
					
				case 10:
					$scope.fields.informed = "1";
					$scope.fields.uninformed = "0";
					break;						
					
					
				case 11:
					$scope.fields.informed = "0";
					break;						

			}      
        }
		
		$scope.resetForm = function()
		{
			$scope.fields =
			{
				"tabletid" : parseInt(tabletId),			
				"underage" : "0",
				"parentunder" : "0",
				"nonsmoker" : "0",
				"smoker" : "0",
				"uninformed" : "0",
				"informed" : "0",
				"send" : "1"
			}		
		}
		
		$scope.sendToServer = function(success)
		{
			$http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded; charset=UTF-8';

			$http.post($rootScope.Host+'/register_first.php',$scope.fields)
			.success(function(data, status, headers, config)
			{
				$scope.resetForm();
				if (success == 1)
				window.location.href="#/app/main";
			})
			.error(function(data, status, headers, config)
			{
				$scope.resetForm();
			});				
		}
		
		$scope.saveOption = function()
		{
			if ($scope.fields.underage == 1)
			{
				$scope.sendToServer(0);	
			}
			else if ($scope.fields.parentunder == 1)
			{
				$scope.sendToServer(0);
			}
			else if ($scope.fields.nonsmoker == 1)
			{
				$scope.sendToServer(0);	
			}	
			
			else if ($scope.fields.smoker == 1 && $scope.fields.uninformed == 0 && $scope.fields.informed == 0)
			{
				$ionicPopup.alert({
				 title: 'יש לבחור האם מאשר לקבל פרטים',
				 template: '',
				 okText : 'אישור'
			   });	
			}

			

			else if ($scope.fields.smoker == 1)
			{
				if ($scope.fields.informed == 1)
				{
					$scope.sendToServer(1);	
				}		
				else
				{
					$scope.sendToServer(0);				

				}
				
			}
			else
			{
				$ionicPopup.alert({
				 title: 'יש לבחור אפשרות',
				 template: '',
				 okText : 'אישור'
			   });	
			}
		}

    })

;
